# Generated by Django 2.2.3 on 2019-09-09 03:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ifor', '0008_auto_20190903_0857'),
    ]

    operations = [
        migrations.CreateModel(
            name='FiturImp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fitur_imp', models.FileField(default='coba.csv', upload_to='iForest/fiturImportance/')),
                ('dataset', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ifor.Dataset')),
                ('setlabel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ifor.SetLabel')),
            ],
        ),
    ]
