# membuat form
from django import forms

from .models_fiturImp import FiturImp as FiturImpModel


class FiturImpForm(forms.ModelForm):
    error_css_class = 'error'

    class Meta:
        model = FiturImpModel
        fields = (
            'dataset',
            'setlabel'
        )

        labels = {
            'setlabel': ' Label(y)',
        }

        widgets = {
            'dataset': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
            'setlabel': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
        }
