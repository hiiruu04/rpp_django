import random
import io
from pylab import savefig
from django.shortcuts import render

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.views.generic import ListView, DetailView, FormView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.http import JsonResponse
from django.core.serializers.json import DjangoJSONEncoder

from .models_dataset import Dataset as DatasetModel
from .models_setLabel import SetLabel as SetLabelModel
from .models_setFitur import SetFitur as SetFiturModel
from .models_fiturImp import FiturImp as FiturImpModel
# from .models_hyperparameterRF import HyperparameterRF as HyperparameterRFModel
# from .models_randomForest import RandomForest as RandomForestModel
from .forms_fiturImp import FiturImpForm
from . import views

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import mpld3
import json
from sklearn.ensemble import RandomForestClassifier
import seaborn as sns
sns.set()


class FiturImpListView(ListView):
    model = FiturImpModel
    ordering = ['id']
    template_name = "ifor/fiturImp/index.html"
    context_object_name = 'fiturImps'

    extra_context = {
        'page_judul': 'Tabel Fitur Importance',
        'page_deskripsi': ' ',
        'page_role': 'Fitur Importance',
    }

    def get_queryset(self):
        count_default_dataset = DatasetModel.objects.filter(
            default_dataset=True).count()
        queryset = SetLabelModel()
        if count_default_dataset == 1:
            default_dataset = DatasetModel.objects.get(
                default_dataset=True)
            queryset = FiturImpModel.objects.filter(
                dataset_id=default_dataset.id)
        return queryset

    def get_context_data(self, *args, **kwargs):
        count_default_dataset = DatasetModel.objects.filter(
            default_dataset=True).count()
        # print(count_default_dataset)

        kwargs.update(self.extra_context)
        context = super(FiturImpListView,
                        self).get_context_data(*args, **kwargs)

        if count_default_dataset == 1:
            default_dataset = DatasetModel.objects.get(
                default_dataset=True)

            count_fiturImp = FiturImpModel.objects.filter(
                dataset_id=default_dataset.id).count()
            context['count_fiturImp'] = count_fiturImp

        context['count_default_dataset'] = count_default_dataset
        return context


class FiturImpCreateView(SuccessMessageMixin, CreateView):
    # model = SetLabelModel
    form_class = FiturImpForm
    template_name = "ifor/fiturImp/create.html"
    success_url = reverse_lazy('if:fiturImp-index')
    context_object_name = 'forms'

    extra_context = {
        'page_judul': 'Tambah Fitur Importance',
        'page_deskripsi': 'Untuk Menghitung Skor Fitur Menggunakan Fungsi Fitur Importance Pada Random Forest',
        'page_role': 'Fitur Importance',
    }

    def get_context_data(self, *args, **kwargs):
        count_default_dataset = DatasetModel.objects.filter(
            default_dataset=True).count()

        kwargs.update(self.extra_context)
        context = super(FiturImpCreateView,
                        self).get_context_data(*args, **kwargs)

        if count_default_dataset == 1:
            get_dataset = DatasetModel.objects.get(
                default_dataset=True)
            get_label = SetLabelModel.objects.filter(
                validate_label=True).filter(
                dataset_id=get_dataset.id).first()
            context['get_dataset'] = get_dataset
            context['get_label'] = get_label
        
        return context
    def post(self, request, **kwargs):
        dataset_id = request.POST.get('dataset')
        setlabel_id = request.POST.get('setlabel')

        get_dataset = DatasetModel.objects.get(
            pk=dataset_id)
        get_label = SetLabelModel.objects.get(
            pk=setlabel_id)

        FI_file = 'media/iForest/fiturImportance/coba.csv'
        FI_file = FI_file.replace('coba',str(dataset_id))

        FI_save = 'iForest/fiturImportance/coba.csv'
        FI_save = FI_save.replace('coba',str(dataset_id))        

        df = views.dataframe(get_dataset.file_dataset, get_dataset.separator)

        df_copy = df.copy()
        # df_copy = df_copy.sample(frac=1)

        # amount of fraud classes 492 rows.
        fraud_df = df_copy.loc[df_copy[get_label.kolom_label] == 1]
        # non_fraud_df = df_copy.loc[df_['Class'] == 0][:492]
        non_fraud_df = df_copy.loc[df_copy[get_label.kolom_label] == 0].sample(n=492,random_state=1)

        normal_distributed_df = pd.concat([fraud_df, non_fraud_df])

        # Shuffle dataframe rows
        df_new = normal_distributed_df.sample(frac=1, random_state=1)
        df_new = df_new.reset_index()
        df_new = df_new.drop('index', axis=1)

        y = df_new[get_label.kolom_label]
        X = df_new.drop([get_label.kolom_label], axis=1)

        n_tree = 10
        clf = RandomForestClassifier(random_state=1,n_estimators=n_tree, max_features='sqrt')
        clf.fit(X,y)

        importances = clf.feature_importances_
        indices = np.argsort(importances)[::-1]
        fitur_importance =[]
        for f in range(X.shape[1]):
            if  importances[indices[f]] > 0 :
                fitur_importance.append([X.columns[indices[f]],importances[indices[f]]])

        df_FI = pd.DataFrame(data=fitur_importance,columns=['fitur','value'])
        df_FI.to_csv(FI_file, index=False)

        fimp = FiturImpModel()
        fimp.dataset_id = dataset_id
        fimp.setlabel_id = setlabel_id
        fimp.fitur_imp = FI_save
        fimp.save()

        return HttpResponseRedirect(reverse_lazy('if:fiturImp-index'))

    def get_success_message(self, cleaned_data):
        return 'Fitur Importance berhasil ditambahakan'

class FiturImpDetailView(DetailView):
    model = FiturImpModel
    template_name = "ifor/fiturImp/detail.html"
    context_object_name = 'fiturImp'

    extra_context = {
        'page_judul': 'Detail Fitur Importance',
        'page_deskripsi': 'untuk melihat detail data Fitur Importance',
        'page_role': 'FiturImp'
    }

    def get_context_data(self, *args, **kwargs):
        fimp = FiturImpModel.objects.get(
            pk=self.kwargs.get('pk'))


        kwargs.update(self.extra_context)
        context = super(FiturImpDetailView,
                        self).get_context_data(*args, **kwargs)


        # get_dataset = rf.dataset
        get_label = fimp.setlabel
        dataset = fimp.dataset

        df = views.dataframe(dataset.file_dataset, dataset.separator)
        # df = views.dataframe(
        #     get_dataset.file_dataset, get_dataset.separator)

        context['dataset_shape'] = df.shape
        context['label_label'] = list(df[fimp.setlabel.kolom_label].unique())
        context['label_frekuensi'] = list(df[fimp.setlabel.kolom_label].value_counts())

        df_result = views.dataframe(fimp.fitur_imp,'koma')
        # df_result.insert(loc=0, column='No', value=list(range(1,df_result.shape[0]+1)))
        # df_result = df_result.set_index('No')
        # df_result = df_result.append(df_result.describe()[1:2])
        context['df_result'] = df_result.to_html(
             classes='dataframe-style table')

        # df_FI = views.dataframe(rf.rf_fitur_importance,'koma')
        # df_FI.insert(loc=0, column='No', value=list(range(1,df_FI.shape[0]+1)))
        # df_FI = df_FI.set_index('No')
        # del df_FI.index.name
        # context['df_FI'] = df_FI.to_html(
        #      classes='dataframe-style table')
        return context

# def set_default(request, pk):
#     if request.method == 'POST':
#         setLabel = SetLabelModel.objects.get(pk=pk)
#         dataset = setLabel.dataset
#         df = views.dataframe(dataset.file_dataset, dataset.separator)
#         if (len(df[setLabel.kolom_label].unique()) != 2):
#             return JsonResponse('warning', safe=False)
#         else:
#             # all_setLabel = SetLabelModel.objects.all()
#             # all_setLabel.update(validate_label=False)

#             setLabel.validate_label = True
#             setLabel.save()

#             dataset.set_label = True
#             dataset.save()

#             return JsonResponse('success', safe=False)


# def get_label(request, id_dataset):
#     dataset = DatasetModel.objects.get(pk=id_dataset)
#     df = views.dataframe(dataset.file_dataset, dataset.separator)

#     dataset_serialized = json.dumps(list(df.columns), cls=DjangoJSONEncoder)

#     return JsonResponse(dataset_serialized, safe=False)


# def get_label_fraud(request, id_dataset, kolom_label):
#     dataset = DatasetModel.objects.get(pk=id_dataset)
#     df = views.dataframe(dataset.file_dataset, dataset.separator)
#     val_label = df[kolom_label].unique()

#     if val_label.dtype != 'object':
#         val_label = val_label.astype('object')

#     # print(val_label.dtype)
#     val_label_serialized = json.dumps(list(val_label), cls=DjangoJSONEncoder)

#     return JsonResponse(val_label_serialized, safe=False)
