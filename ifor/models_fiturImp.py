from django.db import models
from django.utils.text import slugify
from .models_dataset import Dataset as DatasetModel
from .models_setLabel import SetLabel as SetLabelModel

class FiturImp(models.Model):
    # reduksi_null_fitur = models.BooleanField(default=False)
    dataset = models.ForeignKey(
        DatasetModel, on_delete=models.CASCADE)
    setlabel = models.ForeignKey(
        SetLabelModel, on_delete=models.CASCADE)
    fitur_imp = models.FileField(
        upload_to='iForest/fiturImportance/', default='coba.csv')
    # reduksi_nilai_kurang_dari = models.CharField(max_length=255,default=0)

    def __str__(self):
        return "[{}] -> {}".format(self.id, self.dataset)
